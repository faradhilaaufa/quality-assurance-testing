import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver


Mobile.startApplication('D:\\QA\\FOLKATECH\\Apk\\chataja-indonesia-messenger-lifestyle-app_5.2.4.apk', true)

scroll_height = Mobile.getDeviceHeight()

scroll_width = Mobile.getDeviceWidth()

Mobile.delay(5)

Mobile.tap(findTestObject('Login/btn_Lanjut'), 0)

Mobile.tap(findTestObject('Login/btn_Lanjut'), 0)

Mobile.tap(findTestObject('Login/btn_Mulai'), 0)

Mobile.setText(findTestObject('Login/txt_inputNumber'), '82218356927', 3)

Mobile.waitForElementPresent(findTestObject('Login/android.widget.Button - LANJUTKAN'), 0)

Mobile.tap(findTestObject('Login/android.widget.Button - LANJUTKAN'), 0)

swipe_width = (scroll_width * 0.5)

x_start = (scroll_height * 0.7)

x_end = (scroll_height * 0.2)

Mobile.swipe(swipe_width.toInteger(), x_start.toInteger(), swipe_width.toInteger(), x_end.toInteger())

Mobile.tap(findTestObject('Login/btn_checkboxSyaratLogin'), 0)

Mobile.delay(5)

Mobile.openNotifications()

Mobile.delay(5)

otp_sms = Mobile.getText(findTestObject('Login/get_otp_from_notification'), 0)

otp_sms = otp_sms.substring(20)

otp_number = otp_sms.replaceAll('\\D+', '')

Mobile.closeNotifications()

Mobile.setText(findTestObject('Login/txt_inputOTP'), otp_number, 0)

Mobile.tap(findTestObject('Login/android.widget.Button - LANJUTKAN OTP'), 0)

Mobile.delay(10)

Mobile.closeApplication()

