import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('ChatAja/Web/Login/txt_inputPhoneNumber'), '85156269869')

WebUI.click(findTestObject('ChatAja/Web/Login/btn_signin'))

//Mobile.openNotifications()
//
//TestObject notification = findTestObject('null')
//
//Mobile.tap(notification, 10)
//
//TestObject otp = findTestObject('null')
//
//String otpText = Mobile.getText(otp, 10)
//
//Mobile.closeNotifications()
WebUI.setText(findTestObject('ChatAja/Web/Login/txt_inputOTP'), '123456')

WebUI.click(findTestObject('ChatAja/Web/Login/btn_confirmCodeOTP'))

WebUI.delay(3)

WebUI.click(findTestObject('ChatAja/Web/Contact/btn_logoContact'))

WebUI.setText(findTestObject('ChatAja/Web/Contact/txt_searchContact'), 'abcd')

WebUI.click(findTestObject('ChatAja/Web/Contact/btn_xSearch'))

WebUI.delay(2)

WebUI.click(findTestObject('ChatAja/Web/Contact/btn_star'))

WebUI.delay(5)

WebUI.click(findTestObject('ChatAja/Web/Contact/btn_favoriteContacts'))

WebUI.closeBrowser()

