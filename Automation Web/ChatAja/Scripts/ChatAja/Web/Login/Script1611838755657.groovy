import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('ChatAja/Web/Login/txt_inputPhoneNumber'), '85156269869')

WebUI.click(findTestObject('ChatAja/Web/Login/btn_signin'))

WebUI.setText(findTestObject('ChatAja/Web/Login/txt_inputOTP'), '123456')

WebUI.click(findTestObject('ChatAja/Web/Login/btn_confirmCodeOTP'))

WebUI.delay(3)

//WebUI.openNotifications()
//WebUI.delay(5)

//WebUI.openBrowser(GlobalVariable.messageUrl, FailureHandling.STOP_ON_FAILURE)
//
//otp_sms = Mobile.getText(findTestObject('Login/get_otp_text'), 0)
//
//otp_sms = otp_sms.substring(20)
//
//otp_number = otp_sms.replaceAll('\\D+', '')
//
//WebUI.setText(findTestObject('ChatAja/Web/Login/txt_inputOTP'), otp_number, 0)
//
//WebUI.click(findTestObject('ChatAja/Web/Login/btn_confirmCodeOTP'))
//
//WebUI.delay(10)

//WebUI.verifyElementVisible(findTestObject('ChatAja/Web/Login/btn_verifyLogoChatAja'), FailureHandling.STOP_ON_FAILURE)
WebUI.closeBrowser()

