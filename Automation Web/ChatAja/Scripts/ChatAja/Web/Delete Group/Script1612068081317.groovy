import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/ChatAja/Web/Login/txt_inputPhoneNumber'), '85156269869')

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Login/btn_signin'))

WebUI.setText(findTestObject('Object Repository/ChatAja/Web/Login/txt_inputOTP'), '123456')

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Login/btn_confirmCodeOTP'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_groupTesting'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_settingGroup'))

WebUI.click(findTestObject('ChatAja/Web/Delete Group/btn_roomInfoGroup'))

WebUI.scrollToElement(findTestObject('ChatAja/Web/Delete Group/btn_contactInfoAufa'), 0)

WebUI.click(findTestObject('ChatAja/Web/Delete Group/btn_contactInfoAufa'))

WebUI.click(findTestObject('ChatAja/Web/Delete Group/btn_expandRemove'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_removeMember'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_yesConfirmation'))

WebUI.scrollToElement(findTestObject('ChatAja/Web/Delete Group/btn_contactInfoArief'), 0)

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_contactInfoArief'))

WebUI.click(findTestObject('ChatAja/Web/Delete Group/btn_expandRemove'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_removeMember'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_yesConfirmation'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_leaveGroup'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/btn_yesConfirmation'))

WebUI.closeBrowser()

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/div_testing'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/img'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/a_Room Info'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/img_1'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/h5_Arief'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/i_Contact Info_ti-close'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/h5_Aufa'))

WebUI.click(findTestObject('Object Repository/ChatAja/Web/Delete Group/delete/Page_(6) New Messages/i_Contact Info_ti-close'))

