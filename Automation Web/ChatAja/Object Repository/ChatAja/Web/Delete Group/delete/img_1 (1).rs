<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_1 (1)</name>
   <tag></tag>
   <elementGuidId>1b0de0d9-0be5-478c-ba8d-ce121353d6d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='contact-information']/div/div[5]/ul[2]/li[2]/div/div/div/div/a/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.users-list-action.action-toggle.mr-2 > div.dropdown > a > img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/img/icon_dropdown_arrow.b3e3b6a6.svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;contact-information&quot;)/div[@class=&quot;sidebar-body&quot;]/div[@class=&quot;p1point5rem groupedMaxHeight&quot;]/ul[@class=&quot;list-group list-group-flush&quot;]/li[@class=&quot;list-group-item p-h-0 list-participants pl-4 pr-4&quot;]/div[@class=&quot;users-list-body align-self-center flex-row&quot;]/div[@class=&quot;p-0&quot;]/div[@class=&quot;users-list-action action-toggle mr-2&quot;]/div[@class=&quot;dropdown&quot;]/a[1]/img[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='contact-information']/div/div[5]/ul[2]/li[2]/div/div/div/div/a/img</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div/div/div/a/img</value>
   </webElementXpaths>
</WebElementEntity>
